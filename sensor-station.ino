#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <IRremote.h>
#include <dht11.h>

/*
 * DISPLAY
 */
LiquidCrystal_I2C lcd(0x20,16,2);
bool lcd_on = true;
uint8_t char_arrow_up_def[8]  = {B00000, B00000, B00000, B00100, B01110, B11111, B00000, B00000};
uint8_t char_arrow_up_neg_def[8]  = {B11111, B11111, B11111, B11011, B10001, B00000, B11111, B11111};
uint8_t char_arrow_down_def[8]  = {B00000, B00000, B00000, B11111, B01110, B00100, B00000, B00000};
uint8_t char_arrow_down_neg_def[8]  = {B11111, B11111, B11111, B00000, B10001, B11011, B11111, B11111};
uint8_t char_battery_cell_full_def[8]  = {B11111, B00000, B11111, B11111, B11111, B11111, B00000, B11111};
uint8_t char_battery_cell_empty_def[8]  = {B11111, B00000, B00000, B00000, B00000, B00000, B00000, B11111};
#define CHAR_ARROW_UP 0
#define CHAR_ARROW_UP_NEG 1
#define CHAR_ARROW_DOWN 2
#define CHAR_ARROW_DOWN_NEG 3
#define CHAR_BATTERY_CELL_FULL 4
#define CHAR_BATTERY_CELL_EMPTY 5
/*
 * DISPLAY MODES
 */
#define DISPLAY_MODE_HT 0
#define DISPLAY_MODE_SND 1
#define DISPLAY_MODE_LIGHT 2
#define DISPLAY_MODE_FLAME 3
#define DISPLAY_MODE_GAS 4
#define DISPLAY_MODE_BATTERY 5
#define N_DISPLAY_MODES 6

int selected_display_mode = DISPLAY_MODE_HT;

/*
 * SENSORS
 */
#define S_IR_PIN 2
IRrecv s_ir(S_IR_PIN);
decode_results remote_button;
#define REMOTE_BTN_UP 0xFD50AF
#define REMOTE_BTN_DOWN 0xFD10EF
#define REMOTE_BTN_POWER 0xFD00FF

dht11 s_ht;
#define S_HT_PIN 4

#define S_SND_PIN 0
int s_snd_value;

#define S_LIGHT_PIN 1
int s_light_value;

#define S_FLAME_PIN 2
#define S_FLAME_THRESHOLD 500
int s_flame_value;

#define S_GAS_PIN 3
#define S_GAS_THRESHOLD 340
int s_gas_value;

#define BUZZER_PIN 8

/*
 * BATTERY
 */
double battery_voltage;
#define BATTERY_MAX_VOLTAGE 4.9
#define BATTERY_MIN_VOLTAGE 3.0
#define BATTERY_DISPLAY_CELLS 14
#define BATTERY_DISPLAY_CELL_RANGE ((BATTERY_MAX_VOLTAGE - BATTERY_MIN_VOLTAGE) / BATTERY_DISPLAY_CELLS)

void setup() {
  Serial.begin(9600);

  init_lcd();
  init_s_ir();
}

void loop() {
  read_s_ht();
  read_s_snd();
  read_s_light();
  read_s_flame();
  read_s_gas();
  read_battery();
  
  read_s_ir();

  switch (selected_display_mode) {
    case DISPLAY_MODE_HT:
      display_s_ht();
      break;
    case DISPLAY_MODE_SND:
      display_s_snd();
      break;
   case DISPLAY_MODE_LIGHT:
      display_s_light();
      break;
   case DISPLAY_MODE_FLAME:
      display_s_flame();
      break;
   case DISPLAY_MODE_GAS:
      display_s_gas();
      break;
   case DISPLAY_MODE_BATTERY:
      display_battery();
      break;
  }
    
  delay(500);
}

/*
 * INIT FUNCTIONS
 */
void init_lcd() {
  lcd.init();
  lcd.backlight();

  lcd.createChar(CHAR_ARROW_UP, char_arrow_up_def);
  lcd.createChar(CHAR_ARROW_UP_NEG, char_arrow_up_neg_def);
  lcd.createChar(CHAR_ARROW_DOWN, char_arrow_down_def);
  lcd.createChar(CHAR_ARROW_DOWN_NEG, char_arrow_down_neg_def);
  lcd.createChar(CHAR_BATTERY_CELL_FULL, char_battery_cell_full_def);  
  lcd.createChar(CHAR_BATTERY_CELL_EMPTY, char_battery_cell_empty_def);
}

void init_s_ir() {
  s_ir.enableIRIn();
}

/*
 * READ FUNCTIONS
 */
void read_s_ht() {
  int chk;
  Serial.print("DHT11, \t");
  chk = s_ht.read(S_HT_PIN);
  switch (chk) {
    case DHTLIB_OK:
      Serial.print("OK,\t");
      break;
    case DHTLIB_ERROR_CHECKSUM:
      Serial.print("Checksum error, \t");
      break;
    case DHTLIB_ERROR_TIMEOUT:
      Serial.print("Time out error, \t");
      break;
    default:
      Serial.print("Unknown error, \t");
      break;
  }
}

void read_s_snd() {
  s_snd_value = analogRead(S_SND_PIN);
}

void read_s_flame() {
  s_flame_value = analogRead(S_FLAME_PIN);

  if (s_flame_value >= S_FLAME_THRESHOLD) {
    tone(BUZZER_PIN, 1000);
  } else {
    noTone(BUZZER_PIN);
  }
}

void read_s_gas() {
  s_gas_value = analogRead(S_GAS_PIN);

  if (s_gas_value >= S_GAS_THRESHOLD) {
    tone(BUZZER_PIN, 2000);
  } else {
    noTone(BUZZER_PIN);
  }
}

void read_s_ir() {
  if (s_ir.decode(&remote_button)) {
    Serial.println(remote_button.value, HEX);

    if (remote_button.value == REMOTE_BTN_UP) {
      selected_display_mode += 1;
      if (selected_display_mode == N_DISPLAY_MODES) {
        selected_display_mode = 0;
      }
      display_on();
      display_print_arrow_blank(remote_button.value);
    }

    if (remote_button.value == REMOTE_BTN_DOWN) {
      selected_display_mode -= 1;
      if (selected_display_mode == -1) {
        selected_display_mode = N_DISPLAY_MODES - 1;
      }
      display_on();
      display_print_arrow_blank(remote_button.value);
    }

    if (remote_button.value == REMOTE_BTN_POWER) {
      if (lcd_on) {
        display_off();
      } else {
        display_on();
      }
    }

    s_ir.resume();
  }
}

void read_s_light() {
  s_light_value = analogRead(S_LIGHT_PIN);
}

void read_battery() {
  long result;
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2);
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL;
  result |= ADCH << 8;
  result = 1126400L / result;
  battery_voltage = double(result) / 1000;
}

 /*
  * DISPLAY FUNCTIONS
  */
void display_s_ht() {
  lcd.clear();

  lcd.print("Temp (C): ");
  lcd.print(s_ht.temperature, 1);

  lcd.setCursor(0, 1);
  lcd.print("Hum (%): ");
  lcd.print(s_ht.humidity, 1);

  display_print_arrows();
}

void display_s_snd() {
  lcd.clear();

  lcd.print("Sound: ");
  lcd.print(s_snd_value);

  lcd.setCursor(0, 1);
  if (s_snd_value > 10) {
    lcd.print("HIGH");
  } else {
    lcd.print("LOW");
  }

  display_print_arrows();
}

void display_s_light() {
  lcd.clear();
  lcd.print("Light: ");
  lcd.print(s_light_value, 1);

  display_print_arrows();
}

void display_s_flame() {
  lcd.clear();
  lcd.print("Flame: ");
  lcd.print(s_flame_value, 1);

  if (s_flame_value >= S_FLAME_THRESHOLD) {
    lcd.setCursor(0, 1);
    lcd.print("FIRE!!!");
  }

  display_print_arrows();
}

void display_s_gas() {
  lcd.clear();
  lcd.print("Gas: ");
  lcd.print(s_gas_value, 1);

  if (s_gas_value >= S_GAS_THRESHOLD) {
    lcd.setCursor(0, 1);
    lcd.print("TOXIC GAS!!!");
  }

  display_print_arrows();
}

void display_battery() {
  lcd.clear();
  lcd.print("Battery: ");
  lcd.print(battery_voltage, 2);
  lcd.setCursor(13, 0);
  lcd.print("V");

  int n_full_cells = (battery_voltage - BATTERY_MIN_VOLTAGE) / BATTERY_DISPLAY_CELL_RANGE;
  n_full_cells = (n_full_cells > BATTERY_DISPLAY_CELLS) ? BATTERY_DISPLAY_CELLS : n_full_cells;

  lcd.setCursor(0, 1);
  for (int i = 0 ; i < n_full_cells ; i++) {
    lcd.print(char(CHAR_BATTERY_CELL_FULL));
  }

  for (int i = n_full_cells ; i < BATTERY_DISPLAY_CELLS ; i++) {
    lcd.print(char(CHAR_BATTERY_CELL_EMPTY));
  }
  
  display_print_arrows();
}

/*
 * LCD DISPLAY HELPER FUNCTIONS
 */
void display_on() {
  lcd.display();
  lcd.backlight();
  lcd_on = true;
}

void display_off () {
  lcd.noDisplay();
  lcd.noBacklight();
  lcd_on = false;
}

void display_print_arrows() {
  lcd.setCursor(15, 0);
  lcd.print(char(CHAR_ARROW_UP));
  lcd.setCursor(15, 1);
  lcd.print(char(CHAR_ARROW_DOWN));
}

void display_print_arrow_blank(unsigned long btn) {
  if (btn == REMOTE_BTN_UP) {
    lcd.setCursor(15, 0);
    lcd.print(char(CHAR_ARROW_UP_NEG));
    delay(100);
    lcd.print(char(CHAR_ARROW_UP));
    delay(100);
  } else {
    lcd.setCursor(15, 1);
    lcd.print(char(CHAR_ARROW_DOWN_NEG));
    delay(100);
    lcd.print(char(CHAR_ARROW_DOWN));
    delay(100);
  }
}
